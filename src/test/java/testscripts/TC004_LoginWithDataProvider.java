package test.java.testscripts;


import main.java.com.airtel.driver.InitializerClass;
import main.java.com.airtel.helpers.ExcelReader;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TC004_LoginWithDataProvider extends InitializerClass{


    @Test(dataProvider = "loginData")
    public void TestLoginWithDataProvider(String Email, String Password, String UserName) throws InterruptedException{
        System.out.println(Email);
        System.out.println(Password);
        System.out.println(UserName);

    }

    @DataProvider(name = "loginData")
    public Object[][] loginData(){
        Object[][] data = getData("TestData.xlsx", "Sheet1");
        return data;
    }

    public Object[][] getData(String ExcelPathName, String SheetName) {
        ExcelReader Data = new ExcelReader(System.getProperty("user.dir")+"/src/main/java/com.airtel.testdata/"+ExcelPathName);
        int rowNum = Data.getRowCount(SheetName);
        System.out.println(rowNum);
        int colNum = Data.getColumnCount(SheetName);
        System.out.println(colNum);
        Object sampleData[][] = new Object[rowNum - 1][colNum];
        for (int i = 2; i <=rowNum; i++) {
            for (int j = 0; j <colNum; j++) {
                sampleData[i - 2][j] = Data.getCellData(SheetName, j, i);
            }
        }
        return sampleData;
    }


}
