package test.java.testscripts;

import main.java.com.airtel.driver.InitializerClass;
import main.java.com.airtel.driver.TestBase;
import main.java.com.airtel.helpers.Utilities;
import main.java.com.airtel.managers.TestManager;
import com.relevantcodes.extentreports.LogStatus;
import main.java.pageObjects.HomePage_POM;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;
import main.java.pageObjects.LoginPage_POM;



public class TC001_VerifyLogin extends InitializerClass {
    TestManager testManager = new TestManager();
    String ExpPageTitle = "Airtel Login : Pay Airtel Bill Online - Login Airtel My Accounts";
    String ExpLoginUrl = "https://www.airtel.in/s/selfcare/";
    String BasePath;
    String SuccessPath;


    @BeforeTest
    @Parameters("browser")
    public synchronized void setUp(String browser) throws Exception {
        loadURL(browser);
        extentLogger = extent.startTest("Browser Initialization");
        extentLogger.log(LogStatus.INFO, browser+" browser is launched ");
        System.out.println(browser+" Browser is selected");
        extentLogger.log(LogStatus.INFO, "Application Url is Passed on the Browser");
        //System.out.println(browser+Thread.currentThread());

        //Creating basefolder to store the screenshots of pass/fail
        Utilities.createFolder("Screenshots");

        // Creating subfolder to store the pass screenshots
        String BasePath = TestBase.setPropFile("BasePathScreenshots");
        Utilities.createSubFolder(BasePath,"Success");
        SuccessPath = TestBase.setPropFile("PassScreenshotsPath");
        testManager.startExtentTest("Application login");
        loginpage = new LoginPage_POM();
        loginpage.loginToApplicaiton(TestBase.setPropFile("MobileNumber"), TestBase.setPropFile("Password"));
        extentLogger.log(LogStatus.PASS," Successfully logged into the application");

        // Calling method for screenshot
        Utilities.getScreenShot( SuccessPath,"LoginPage");
    }


   @Test
    public synchronized void  pageTitleVerify() throws Exception{
        testManager.startExtentTest("TC001_VerifyLogin");
        String ActTPagetitle = driver.getTitle();
        Assert.assertEquals(ActTPagetitle,ExpPageTitle,"the page title is not verified");
        System.out.println("Title is verified");
        extentLogger.log(LogStatus.PASS,"Login verified using title Assertion");


       // Calling method for screenshot
        Utilities.getScreenShot(SuccessPath, "TC001_VerifyTitle");
        // Verifying the LoginClick URL
        Assert.assertTrue(main.java.com.airtel.helpers.Utilities.validateURL(ExpLoginUrl), "Login URL is not as expected");
        System.out.println("LoginClick url is as expected");
        extentLogger.log(LogStatus.PASS, "Login click URL is as expected");

       // Calling method for screenshot
        Utilities.getScreenShot(SuccessPath, "TC001_VerifyUrl");

    }

    @AfterTest
    public void closeApp(){
        endExtentTest();
        testManager.startExtentTest("Application logout");
        homepage = new HomePage_POM();
        homepage.clickLogout();
        System.out.println("Logout Successfully Verified");
        extentLogger.log(LogStatus.PASS,"Logout Successfully Verified");
        driver.quit();
        System.out.println("driver is closed");
        extentLogger.log(LogStatus.PASS, "Driver is closed");
    }


}