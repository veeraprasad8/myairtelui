package test.java.testscripts;

import main.java.com.airtel.driver.InitializerClass;
import main.java.com.airtel.driver.TestBase;
import main.java.com.airtel.helpers.Utilities;
import main.java.com.airtel.managers.TestManager;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.*;
import main.java.pageObjects.HomePage_POM;


public class TC002_VerifyAddMoney extends InitializerClass {

    TestManager testManager = new TestManager();
    String ExpAddMoneyURL = "https://www.airtel.in/s/load-money";
    String SuccessPath;



   @Test
    public synchronized void verifyAddMoneyTest() throws Exception {
        System.out.println("Im in secone test case");
        testManager.startExtentTest("TC002_VerifyTestCaseTwo");
        homepage = new HomePage_POM();
        homepage.clickAddMoney();
        extentLogger.log(LogStatus.INFO, "Clicked on AddMoney link");
        Assert.assertEquals(true, homepage.verifyAddMoney());
        extentLogger.log(LogStatus.PASS, "Add Money is Verified");
        SuccessPath = TestBase.setPropFile("PassScreenshotsPath");

       // Calling method for screenshot
        Utilities.getScreenShot(SuccessPath,"TC002_AddMoney Verify");
        Assert.assertTrue(main.java.com.airtel.helpers.Utilities.validateURL(ExpAddMoneyURL), "Error The URL is not as expected");
        extentLogger.log(LogStatus.PASS, "Add Money URL is as Expected");
        homepage.clickCancelButton();
        extentLogger.log(LogStatus.INFO, "Clicked on Cancel Button");
    }


}