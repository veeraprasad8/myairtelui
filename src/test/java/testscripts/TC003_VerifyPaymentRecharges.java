package test.java.testscripts;

import com.relevantcodes.extentreports.LogStatus;
import main.java.com.airtel.driver.InitializerClass;
import main.java.com.airtel.driver.TestBase;
import main.java.com.airtel.helpers.Utilities;
import main.java.pageObjects.PaymentsRecharges;
import org.testng.Assert;
import org.testng.annotations.Test;
import main.java.com.airtel.managers.TestManager;



public class TC003_VerifyPaymentRecharges extends InitializerClass {

    TestManager testManager = new TestManager();
    String ExpPaymentRechargesText = "Payments & Recharges";
    String ExpPaymentRechargesUrl = "https://www.airtel.in/s/selfcare/payments-recharges";
    String SuccessPath;


    @Test
    public void verifyPaymentRecharges() throws Exception {
        testManager.startExtentTest("TC003_VerifyPaymentRecharges");
        System.out.println("Entered in PaymentRecharges testcase");
        payrecharges = new PaymentsRecharges();
        payrecharges.clickPaymementRechargesVerify();
        extentLogger.log(LogStatus.INFO, "Clicked on PaymentRecharges link");
        String ActHeader = payrecharges.getHeader();
        Assert.assertEquals(ActHeader, ExpPaymentRechargesText);
        extentLogger.log(LogStatus.PASS, "PaymentRecharges SubHeading text is verified");
        SuccessPath = TestBase.setPropFile("PassScreenshotsPath");

        // Calling method for screenshot
        Utilities.getScreenShot(SuccessPath,"TC003_VerifyPaymentText");
        // Verifying the PaymentRecharges Api Url
        Assert.assertTrue(main.java.com.airtel.helpers.Utilities.validateURL(ExpPaymentRechargesUrl), "PaymentRecharges Api Url is not as expected");
        extentLogger.log(LogStatus.PASS, "PaymentRecharges Api Url is verified as expected");

    }
}
