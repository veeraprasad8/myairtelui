package test.java.testscripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;

public class GoogleSearch {

   public static WebDriver driver;
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Drivers/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");
        driver = new ChromeDriver(options);
        driver.get("https://www.google.com");
        driver.findElement(By.id("lst-ib")).sendKeys("selenium");
        List<WebElement> elements=driver.findElements(By.xpath("//ul[@role='listbox']/li/descendant::div[@class='sbqs_c']"));
      // List<WebElement> elements=driver.findElements(By.xpath("//ul[@role='listbox']/li/descendant::div[@class='sbqs_c']"));
        System.out.println("the elements are :"+elements.size());
        for (int i =0;i<elements.size();i++){
            String value = elements.get(i).getText();
            if(value.equals("selenium tutorial")){
                elements.get(i).click();
                // System.out.println("gihtub code");
                break;
            }
        }

    }
}
